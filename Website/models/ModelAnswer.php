<?php

class ModelAnswer
{
    private $gwAnswer;

    public function __construct()
    {
        $this->gwAnswer = new GatewayAnswer();
    }

    function addAnswer($answer)
    {
        $answersId = $this->gwAnswer->addAnswer($answer);
        return $answersId;
    }

    function getAnswersByIDQuestions($id)
    {
        $answers = $this->gwAnswer->getAnswersByIDQuestions($id);
        return $answers;
    }

    function updateAnswer($answersId, $answer)
    {
        $this->gwAnswer->updateAnswer($answersId, $answer);
    }
}
