<?php

class GatewayLobby
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addLobby($lobby)
    {
        $query = "insert into Lobbies(id,name,password,nbPlayer) values (:id,:name,:password,:nbPlayer);";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($lobby->getId(), PDO::PARAM_INT),
                ':name' => array($lobby->getName(), PDO::PARAM_STR),
                ':password' => array($lobby->getPassword(), PDO::PARAM_STR),
                ':nbPlayer' => array($lobby->getNbPlayer(), PDO::PARAM_INT)
            )
        );
    }

    public function getLobbys()
    {
        $query = "SELECT * FROM Lobbies;";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();
        if ($results == NULL) {
            return false;
        }
        $lobbys = array();
        foreach ($results as $row) {
            $lobbys[] = new Lobby($row['id'], $row['name'], $row['password'], $row['nbPlayer']);
        }
        return $lobbys;
    }

    public function getLobbyByName($name)
    {
        $query = "SELECT * FROM Lobbies WHERE name = :name;";
        $this->con->executeQuery($query, array(':name' => array($name, PDO::PARAM_STR)));
        $results = $this->con->getResults();
        if ($results == NULL) {
            return false;
        }
        return new Lobby($results[0]['id'], $results[0]['name'], $results[0]['password'], $results[0]['nbPlayer']);
    }


    public function deleteLobby($id)
    {
        $query = "DELETE FROM Lobbies WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
    }


}
