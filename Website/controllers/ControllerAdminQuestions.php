<?php

class ControllerAdminQuestions
{
    private $mdQuestion;
    private $mdChapter;
    private $mdAnswer;

    private $twig;
    private $vues;

    function __construct()
    {
        global $dns, $user, $pass, $vues, $twig;
        session_start();
        try {
            if($_SESSION["idAdminConnected"] != null){
                $this->twig =$twig;
                $this->vues = $vues;

                $this->mdQuestion = new ModelQuestion();
                $this->mdAnswer = new ModelAnswer();
                $this->mdChapter = new ModelChapter();

                $questions = $this->mdQuestion->getQuestions();
                $chapters = $this->mdChapter->getChapters();

                echo $twig->render($vues["adminQuestions"], [
                    'questions' => $questions,
                    'chapters' => $chapters,
                ]);
            }
            else {
                header("Location:/login");
            }
        } catch (PDOException $e) {
            // Gérez les erreurs PDO ici
        } catch (Exception $e2) {
            // Gérez d'autres erreurs ici
        }
    }

    function delete($param) {
        $this->mdQuestion->deleteQuestionByID($param["id"]);
        header("Location:/admin/questions");
    }

    function add($param) {
        $content = $_POST['content'];
        $idChapter = intval($_POST['idChapter']);
        $AnswersPost = array();
        $AnswersPost[0] = $_POST['answer1'];
        $AnswersPost[1] = $_POST['answer2'];
        $AnswersPost[2] = $_POST['answer3'];
        $AnswersPost[3] = $_POST['answer4'];
        $correctAnswer = intval($_POST['correctAnswer']);

        $Question = [
            'content' => $content,
            'idchapter' => $idChapter,
            'difficulty' => 1,
            'nbfails' => 0,
        ];

        $idquestion = intval($this->mdQuestion->addQuestion($Question));

        for ($i = 0; $i <= 3; $i++) {
            $Answers[] = [
                'content' => $AnswersPost[$i],
                'idquestion' => $idquestion,
            ];
        }


        $answersId = array();
        for ($i = 0; $i <= 3; $i++) {
            $answersId[$i] = $this->mdAnswer->addAnswer($Answers[$i]);
        }

        $Question = [
            'content' => $content,
            'idchapter' => $idChapter,
            'difficulty' => 1,
            'nbfails' => 0,
            'idanswergood' => $answersId[$correctAnswer],
        ];

        $this->mdQuestion->updateQuestion($idquestion,$Question);
        
        header("Location:/admin/questions");
    }

    function updatemodal($param) {

        $question = $this->mdQuestion->getQuestionByID($param["id"]);

        $answers = array();
        $answers = $this->mdAnswer->getAnswersByIDQuestions($param["id"]);

        $chapters = $this->mdChapter->getChapters();

        echo $this->twig->render($this->vues["adminQuestionsModal"], [
            'question' => $question,
            'chapters' => $chapters,
            'answers' => $answers,
        ]);
    }

    function update($param) {

        $id = $_POST['id'];
        $content = $_POST['content'];
        $idChapter = intval($_POST['idChapter']);

        $correctAnswer = intval($_POST['correctAnswer']);

        $answersId = array();
        $answersId[0] = intval($_POST['IdAnswer1']);
        $answersId[1] = intval($_POST['IdAnswer2']);
        $answersId[2] = intval($_POST['IdAnswer3']);
        $answersId[3] = intval($_POST['IdAnswer4']);

        $answers = array();
        $answers[0] = $_POST['answer1'];
        $answers[1] = $_POST['answer2'];
        $answers[2] = $_POST['answer3'];
        $answers[3] = $_POST['answer4'];

        $Question = [
            'content' => $content,
            'idchapter' => $idChapter,
            'idanswergood' => $answersId[$correctAnswer],
        ];

        $this->mdQuestion->updateQuestion($id, $Question);

        for ($i = 0; $i <= 3; $i++) {
            $Answers[] = [
                'content' => $answers[$i],
                'id' => $id,
            ];
        }

        for ($i = 0; $i <= 3; $i++) {
            $this->mdAnswer->updateAnswer($answersId[$i],$Answers[$i]);
        }

        header("Location:/admin/questions");
    }
}