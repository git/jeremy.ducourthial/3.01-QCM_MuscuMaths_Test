<?php

class Answer
{
    private int $id;
    private string $content;
    private int $idQuestion;

    public function __construct(string $content, int $idQuestion)
    {
        $this->content = $content;
        $this->idQuestion = $idQuestion;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getIdQuestion()
    {
        return $this->idQuestion;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }
}
