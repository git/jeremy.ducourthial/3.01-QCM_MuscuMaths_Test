<?php

class ModelQuestion
{
    private $gwQuestion;

    public function __construct()
    {
        $this->gwQuestion = new GatewayQuestion();
    }

    function getQuestions()
    {
        $questions = $this->gwQuestion->getQuestions();
        return $questions;
    }

    function deleteQuestionByID($id)
    {
        $this->gwQuestion->deleteQuestionByID($id);
    }

    function addQuestion($Question)
    {
        $questionId = $this->gwQuestion->addQuestion($Question);
        return $questionId;
    }

    function getQuestionByID($id)
    {
        $question = $this->gwQuestion->getQuestionByID($id);
        return $question;
    }

    function updateQuestion($id, $Question)
    {
        $this->gwQuestion->updateQuestion($id, $Question);
    }

    function getQuestionsByChapterAndDifficulty($chapter, $difficulty)
    {
        $questions = $this->gwQuestion->getQuestionsByChapterAndDifficulty($chapter, $difficulty);
        return $questions;
    }
}
