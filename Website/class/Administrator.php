<?php

class Administrator
{
    private int $id;
    private string $username;
    private string $hashedPassword;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        try {
            $this->hashedPassword = $hashedPassword = md5($password);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function getUsername()
    {
        return $this->username;
    }

    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }
    
    public function setHashedPassword(string $hashedPassword)
    {
        $this->hashedPassword = $hashedPassword;
    }
}
