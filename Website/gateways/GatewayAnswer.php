<?php

class GatewayAnswer
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addAnswer($answer)
    {
        $query = "insert into answers(content,idquestion) values (:content,:idquestion);";
        $this->con->executeQuery(
            $query,
            array(
                ':content' => array($answer['content'], PDO::PARAM_STR),
                ':idquestion' => array($answer['idquestion'], PDO::PARAM_INT),
            )
        );
        $answerId = $this->con->lastInsertId();
        return $answerId;
    }

    public function getAnswersByIDQuestions($idQuestions)
    {
        $query = "SELECT answers.content,answers.id FROM answers, questions WHERE questions.id = :idquestions AND answers.idquestion = questions.id ;";
        $this->con->executeQuery(
            $query,
            array(
                ':idquestions' => array($idQuestions, PDO::PARAM_INT)
            )
        );
        $results = $this->con->getResults();

        return $results;
    }

    public function updateAnswer($id, $answer)
    {
        $query = "UPDATE answers SET content = :content WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT),
                ':content' => array($answer['content'], PDO::PARAM_STR)
            )
        );
    }

    public function deleteAnswer($id)
    {
        $query = "DELETE FROM answers WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
    }

}
