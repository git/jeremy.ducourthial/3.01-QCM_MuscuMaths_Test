<?php

class ModelChapter
{
    private $gwChapter;

    public function __construct()
    {
        $this->gwChapter = new GatewayChapter();
    }

    function getChapters()
    {
        $chapters = $this->gwChapter->getChapters();
        return $chapters;
    }

    function deleteChapter($id) 
    {
        $this->gwChapter->deleteChapter($id);
    }

    function addChapter($chapter) 
    {
        $this->gwChapter->addChapter($chapter);
    }

    function getChapterByID($id) 
    {
        $chapter = $this->gwChapter->getChapterByID($id);
        return $chapter;
    }

    function updateChapter($id,$chapter) 
    {
        $this->gwChapter->updateChapter($id,$chapter);
    }
    public function verifyChapter($chapter)
    {
        $id = $this->gwChapter->verifyChapter($chapter);
        return $id;
    }
}
