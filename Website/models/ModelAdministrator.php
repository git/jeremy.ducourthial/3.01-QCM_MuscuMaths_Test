<?php

class ModelAdministrator
{
    private $gwAdministrator;

    public function __construct()
    {
        $this->gwAdministrator = new GatewayAdministrator();
    }

    public function addAdministrator($administrator)
    {
        $this->gwAdministrator->addAdministrator($administrator);
    }

    public function getAdministratorByID($id)
    {
        $administrator = $this->gwAdministrator->getAdministratorByID($id);
        return $administrator;
    }

    public function getAdministrators()
    {
        $administrators = $this->gwAdministrator->getAdministrators();
        return $administrators;
    }

    public function updateAdministrator($id,$administrator)
    {
        $this->gwAdministrator->updateAdministrator($id,$administrator);
    }

    public function deleteAdministratorByID($id)
    {
        $this->gwAdministrator->deleteAdministratorByID($id);
    }

    public function verifyAdministrator($Administrator)
    {
        $administratorsId = $this->gwAdministrator->verifyAdministrator($Administrator);
        return $administratorsId;
    }
}
