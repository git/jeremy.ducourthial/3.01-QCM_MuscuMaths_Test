# 3.01-QCM_MuscuMaths

LogicSys Digital

Application de QCM maths pour la nouvelle matière de 1er année Maths Muscu

Convention de nommage des commits :

    build : changements qui affectent le système de build ou des dépendances externes (npm, make…)
    ci : changements concernant les fichiers et scripts d'intégration ou de configuration (Travis, Ansible, BrowserStack…)
    feat : ajout d'une nouvelle fonctionnalité
    fix : correction d'un bug
    perf : amélioration des performances
    refactor : modification qui n'apporte ni nouvelle fonctionalité ni d'amélioration de performances
    style : changement qui n'apporte aucune alteration fonctionnelle ou sémantique (indentation, mise en forme, ajout d'espace, renommante d'une variable…)
    docs : rédaction ou mise à jour de documentation
    test : ajout ou modification de tests

    Exemple :
    feat : ajout de la possibilité de se connecter
    perf : optimisation de requête SQL
    ...Etc...


par GUITARD Maxence, VAN BRABRANDT Jade, DUCOURTHIAL Jérémy, CALATAYUD Yvan, NORTIER Damien