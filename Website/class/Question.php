<?php

class Question
{
    private int $id;
    private string $content;
    private int $idChapter;
    private int $difficulty;
    private int $nbFails;
    private int $idAnswerGood;

    public function __construct(string $content, int $idChapter, int $idAnswerGood = -1, int $difficulty = 1, int $nbFails = 0)
    {
        $this->content = $content;
        $this->idChapter = $idChapter;
        if ($idAnswerGood != -1) {
            $this->idAnswerGood = $idAnswerGood;
        }
        $this->difficulty = $difficulty;
        $this->nbFails = $nbFails;
    }

    public function getContent() 
    {
        return $this->content;
    }

    public function getIdChapter()
    {
        return $this->idChapter;
    }

    public function getIdAnswerGood()
    {
        return $this->idAnswerGood;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function setIdAnswerGood(int $idAnswerGood)
    {
        $this->idAnswerGood = $idAnswerGood;
    }

    public function getDifficulty()
    {
        return $this->difficulty;
    }
    
    public function setDifficulty(int $difficulty)
    {
        $this->difficulty = $difficulty;
    }

    public function getNbFails()
    {
        return $this->nbFails;
    }

    public function setNbFails(int $nbFails)
    {
        $this->nbFails = $nbFails;
    }
}
