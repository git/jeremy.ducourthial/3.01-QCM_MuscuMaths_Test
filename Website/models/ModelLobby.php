<?php

class ModelLobby
{
    private $gwLobby;

    public function __construct()
    {
        $this->gwLobby = new GatewayLobby();
    }

    public function addLobby($lobby)
    {
        $this->gwLobby->addLobby($lobby);
    }

    public function getlobbies()
    {
        $lobbies = $this->gwLobby->getlobbies();
        return $lobbies;
    }
}
