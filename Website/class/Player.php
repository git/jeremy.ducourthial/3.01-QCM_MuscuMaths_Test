<?php

class Player
{
    private int $id;
    private string $nickname;
    private string $hashedPassword;

    public function __construct(int $id, string $nickname, string $password)
    {
        $this->id = $id;
        $this->nickname = $nickname;
        try {
            $this->hashedPassword = $hashedPassword = md5($password);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }
    public function setHashedPassword(string $hashedPassword)
    {
        $this->hashedPassword = $hashedPassword;
    }
}
