<?php

class ModelPlayer
{
    private $gwPlayer;

    public function __construct()
    {
        $this->gwPlayer = new GatewayPlayer();
    }

    public function addPlayer($player)
    {
        $this->gwPlayer->addPlayer($player);
    }

    public function getPlayerByID($id)
    {
        $player = $this->gwPlayer->getPlayerByID($id);
        return $player;
    }

    public function updatePlayer($id,$player)
    {
        $this->gwPlayer->updatePlayer($id,$player);
    }

    public function deletePlayerByID($id)
    {
        $this->gwPlayer->deletePlayerByID($id);
    }
}
