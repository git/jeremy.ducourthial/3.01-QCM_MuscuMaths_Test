<?php

class ControllerUser
{
    private $mdQuestion;
    private $mdChapter;
    private $mdAnswer;
    private $mdPlayer;
    private $mdLobby;
    private $mdAdministrator;

    private $twig;
    private $vues;

    function __construct()
    {
        global $vues, $twig;
        session_start();
        try {

            $this->twig = $twig;
            $this->vues = $vues;

            $this->mdQuestion = new ModelQuestion();
            $this->mdAnswer = new ModelAnswer();
            $this->mdChapter = new ModelChapter();
            $this->mdPlayer = new ModelPlayer();
            $this->mdLobby = new ModelLobby();
            $this->mdAdministrator = new ModelAdministrator();
        } catch (PDOException $e) {
            // $dataVueEreur[] = "Erreur inattendue!!! ";
            // require(__DIR__.'/../vues/erreur.php');
        } catch (Exception $e2) {
            // $dataVueEreur[] = "Erreur inattendue!!! ";
            // require ($rep.$vues['erreur']);
        }
    }

    function home()
    {
        echo $this->twig->render($this->vues["home"]);
    }
    function themeChoice()
    {
        $chapters = $this->mdChapter->getChapters();
        echo $this->twig->render($this->vues["themeChoice"], [
            'chapters' => $chapters,
        ]);
    }
    function singleplayer()
    {
        echo $this->twig->render($this->vues["singleplayer"]);
    }

    function multiplayer()
    {
        echo $this->twig->render($this->vues["multiplayer"]);
    }

    function login()
    {
        echo $this->twig->render($this->vues["loginAdmin"], [
            'error' => $_SESSION["error"],
        ]);

        $_SESSION["error"] = "";
    }

    function verifyAdmin()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $Administrator = [
            'username' => $username,
            'password' => $password,
        ];

        $AdministratorIsOk = $this->mdAdministrator->verifyAdministrator($Administrator);
        if ($AdministratorIsOk != null) {
            $_SESSION["idAdminConnected"] = $AdministratorIsOk;
            header("Location:/admin/administrators");
        } else {
            $_SESSION["error"] = "utilisateur introuvable.";
            header("Location:/login");
        }
    }

    function verifySingleplayer()
    {
        $difficulty = $_POST['difficulty'];
        $chapter = $_POST['chapter'];
        $difficultyIsOk = TRUE;
        $chapterIsOk = TRUE;

        if (!($difficulty == 0 or $difficulty == 1 or $difficulty == 2)) {
            $_SESSION["error"] = "Valeur de difficulté invalide";
            $difficultyIsOk = FALSE;
        }

        if ($this->mdChapter->verifyChapter($chapter) == NULL) {
            $_SESSION["error"] = "Valeur de chapitre invalide";
            $chapterIsOk = FALSE;
        }
        if ($difficultyIsOk and $chapterIsOk) {
            $questions = $this->mdQuestion->getQuestionsByChapterAndDifficulty($chapter, $difficulty);
            foreach ($questions as &$question) {
                $answers = $this->mdAnswer->getAnswersByIDQuestions($question['id']);
                $question['answers'] = $answers;
            }
            echo $this->twig->render($this->vues["singleplayer"], [
                'questions' => $questions,
                'numQuestion' => 0,
                'jsonQuestions' => json_encode($questions),
            ]);
        } else {
            $_SESSION["error"] = "Valeur de choix de thème invalide";
            header("Location:/themeChoice");
        }
    }

    function verifQuestion()
    //Only Handdle solo game
    {
        $answerNumber = $_POST["answer"];
        $numQuestion = $_POST["numQuestion"] + 1;
        $questions = json_decode($_POST["questions"], true);
        if ($numQuestion > 9) {
            echo $this->twig->render($this->vues["home"]);
            //EN GROS IL FAUT AFFICHER LE SCORE (C'est copilot qui a fait ça, la fin du commentaire j'veux dire)
            //Si faut paser un param score de page en page dittes le moi je le ferais dw
        } else {

            if ($questions[$numQuestion - 1]['idanswergood'] == $questions[$numQuestion - 1]['answers'][$answerNumber]['id']) {
                // Player won
            } else {
                //Player lost
            }
            echo $this->twig->render($this->vues["singleplayer"], [
                'questions' => $questions,
                'numQuestion' => $numQuestion,
                'jsonQuestions' => json_encode($questions),
            ]);
        }
    }
}
