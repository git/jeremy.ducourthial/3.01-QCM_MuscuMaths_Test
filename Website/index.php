<?php

//chargement config
require_once(__DIR__ . '/usages/Config.php');

//chargement for twig
require __DIR__ . '/vendor/autoload.php';

//chargement autoloader pour autochargement des classes
require_once(__DIR__ . '/usages/Autoload.php');
Autoload::charger();

//twig
$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$controller = new FrontController();
