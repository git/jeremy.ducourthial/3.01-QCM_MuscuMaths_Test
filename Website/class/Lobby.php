<?php

class Lobby
{
    private int $id;
    private string $name;
    private string $password;
    private int $nbPlayer;

    public function __construct(int $id, string $name, string $password, int $nbPlayer)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->nbPlayer = $nbPlayer;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNbPlayer()
    {
        return $this->nbPlayer;
    }
    public function setNbplayer(int $nbPlayer)
    {
        $this->nbPlayer = $nbPlayer;
    }
}
